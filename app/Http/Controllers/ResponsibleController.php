<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResponsibleController extends Controller
{
    public function index()
    {   
        $res = [
            'achievement' => DB::table('achievement')->get(),
            'advantage' => DB::table('advantage')->get(),
            'clients' => DB::table('clients')->get(),
            'fasility' => DB::table('fasility')->get(),
            'fleet' => DB::table('fleet')->get(),
            'portfolio' => DB::table('portfolio')->get(),
            'recomendation' => DB::table('recomendation')->get(),
            'rule' => DB::table('rule')->get(),
            'testi' => DB::table('testi')->get(),
        ];

        return response()->json($res, 200);
    }
}
