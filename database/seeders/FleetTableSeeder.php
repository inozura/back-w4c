<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FleetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fleet')->insert(array(
            array(
              'name' => 'Pick Up',
              'capacity' => '1.5 Ton',
              'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/armada/Pick-Up.jpg'
            ),
            array(
                'name' => 'Light Truck',
                'capacity' => '2.5 Ton',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/armada/Light-Truck.jpg'
            ),
              array(
                'name' => 'Dump Truck',
                'capacity' => '16 Ton',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/armada/Dump-Truck.jpg'
            ),
              array(
                'name' => 'Buck Motor',
                'capacity' => '700 kg',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/armada/Baktor.jpg'
            ),
              array(
                'name' => 'Arm Roll',
                'capacity' => '5 Ton',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/armada/Arm-roll.jpg'
            )
        ));
    }
}
