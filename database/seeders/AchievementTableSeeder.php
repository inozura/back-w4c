<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AchievementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('achievement')->insert([
        'clients' => 6,
        'trash_weight' => 133823,
        'cities' => 7
      ]);
    }
}
