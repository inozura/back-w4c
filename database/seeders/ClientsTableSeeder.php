<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert(array(
            array(
              'name' => 'Binus School Bekasi',
              'logo' => 'https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/client/Binus-School.jpg'
            ),
            array(
                'name' => 'Javara',
                'logo' => 'https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/client/Javara.jpg'
            ),
            array(
                'name' => 'Mang Kabayan',
                'logo' => 'https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/client/Mang-Kabayan.jpg'
            ),
            array(
                'name' => 'Sekolah Seniman Pangan',
                'logo' => 'https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/client/Seniman-Pangan.jpg'
            ),
            array(
                'name' => 'Wisma Barito',
                'logo' => 'https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/client/Wisma-Barito.jpg'
            ),
            array(
                'name' => 'Vida Bekasi',
                'logo' => 'https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/client/Vida-Bekasi.jpg'
            ),
            array(
                'name' => 'Institute Francais Indonesia',
                'logo' => 'https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/client/Institute-Francais-Indonesia.jpg'
            )
        ));
    }
}
