<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rule')->insert(array(
            array(
              'name' => 'Pemilahan sampah oleh konsumen',
              'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/flow/drop.png',
            ),
            array(
                'name' => 'Penyimpanan sampah di gudang/toko klien',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/flow/storage.png',
            ),
            array(
                'name' => 'Pengumpulan sampah terpilah',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/flow/transfer.png',
            ),
            array(
                'name' => 'Pengolahan sampah di Rumah Pemulihan Material W4C',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/flow/process.png',
            ),
            array(
                'name' => 'Residu dikirim ke TPA',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/flow/landfill.png',
            ),
        ));
    }
}
