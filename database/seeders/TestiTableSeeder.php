<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('testi')->insert([
            'url_video' => 'https://www.youtube.com/watch?v=oRD576oYol8'
        ]);
    }
}
