<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            AchievementTableSeeder::class,
            AdvantageTableSeeder::class,
            ClientsTableSeeder::class,
            FasilityTableSeeder::class,
            FleetTableSeeder::class,
            PortfolioTableSeeder::class,
            RecomendationTableSeeder::class,
            RuleTableSeeder::class,
            TestiTableSeeder::class
        ]);
    }
}
