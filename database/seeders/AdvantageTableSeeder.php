<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdvantageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advantage')->insert(array(
            [
                'desc' => 'Pendekatan manajemen sampah yang 100% terpilah'
            ],
            [
                'desc' => 'Mengurangi timbulan sampah yang dibuang ke TPA'
            ],
            [
                'desc' => 'Meningkatkan kepedulian pegawai tentang isu sampah'
            ],
            [
                'desc' => 'Meningkatkan citra perusahaan'
            ],
            [
                'desc' => 'Menaati Peraturan Pemerintah (PP) No. 81 Tahun 2012 tentang Pengelolaan Sampah Rumah Tangga dan Sampah Sejenis Rumah Tangga, serta mendukung Peraturan Presiden Nomor 97 Tahun 2017 (JAKSTRANAS)'
            ]
        ));
    }
}
