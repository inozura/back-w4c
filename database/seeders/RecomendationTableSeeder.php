<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RecomendationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('recomendation')->insert([
            'name' => 'Kawasan Pariwisata & Perbelanjaan',
            'logo' => 'https://waste4change.com/official/2.8.assets/img/icons/kawasanbelanja.png'
        ]);
    }
}
