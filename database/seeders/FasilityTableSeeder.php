<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FasilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fasility')->insert(array(
            array(
              'name' => 'Kantong sampah yang akan mendukung pemilahan sampah',
              'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/what-you-get/wastebin-m.png',
            ),
            array(
                'name' => 'Pelatihan induksi teknis sebanyak satu kali',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/what-you-get/loadspeaker-m.png',
            ),
            array(
                'name' => 'Pelatihan induksi teknis sebanyak satu kali',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/what-you-get/loadspeaker-m.png',
            ),
            array(
                'name' => 'Laporan tentang alur sampah',
                'image_url' => 'https://waste4change.com/official/2.8.assets/img/icons/what-you-get/report-m.png',
            )
        ));
    }
}
