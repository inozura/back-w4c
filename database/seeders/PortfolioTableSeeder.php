<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PortfolioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portfolio')->insert(array(
            array(
              'name' => 'Vida Bekasi',
              'logo' => 'https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/portfolio/vida.jpg',
              'address' => 'Jl. Tirta Utama No. 1, Bumiwedari, Bantar Gebang, Kota Bekasi',
              'duration' => null,
              'signature' => 2014,
              'schedule' => 3,
              'recycled' => 112.000,
            ),
            array(
                'name' => 'Wisma Barito',
                'logo' => 'https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/portfolio/barito.jpg',
                'address' => 'Jl. Letjen. S. Parman Kav. 62-63, Slipi, Jakarta Barat',
                'duration' => 1,
                'signature' => 2019,
                'schedule' => 3,
                'recycled' => 6.922,
              ),
        ));
    }
}
